.. sphinx-demo documentation master file, created by
   sphinx-quickstart on Sun Feb 28 16:04:29 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to weshare-sphinx-demo's documentation!
===============================================

.. toctree::
   :maxdepth: 1

   howto

If you want to dig deeper, see `Official documentation <https://www.sphinx-doc.org/en/master/contents.html>`_

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
